document.title = 'Socket'

const socket = io()
socket.on('connect', () => {
    console.log(socket.id)
})
socket.on('message', msg =>{
    msgBox.append(li(msg))
})

const inputUser = input('text').attr('required', true)
const userForm = form(label(h1('Set username'), inputUser), button('set'))

let username = ''
userForm.onsubmit = e => {
    e.preventDefault()
    if (inputUser.value) {
        username = inputUser.value
        socket.emit('enter', username, socket.id)
        show()
    }
}

const userlist = ul()
socket.on('enter', (user, id) => {
    userlist.append(li(user).attr('id', id))
})
socket.on('leave', id => {
    console.log(id)
    userlist.querySelector(`#${id}`).remove()
})

const inputBox = input('text').attr('name', 'message')
const inputForm = form(inputBox, button('send'))

const msgBox = ul()

inputForm.onsubmit = e => {
    e.preventDefault()
    if (inputBox.value) {
        socket.send(`${username}: ${inputBox.value}`)
        inputBox.value = ''
    }
}

function show() {
    document.body.replaceChildren(
        h2('User: ' + username),
        inputForm,
        div(h3('Message'), msgBox),
        div(h3('Users'), userlist)
    )
}

function setUsername() {
    document.body.replaceChildren(
        userForm
    )
}

function label(...children) {
    return tag('label', ...children)
}

window.onload = () => {
    if (!username) {
        setUsername()
    } else {
        show()
    }
}
