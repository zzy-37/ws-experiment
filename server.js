const express = require('express')
const app = express()
const http = require('http')
const server = http.createServer(app)
const { Server } = require('socket.io')
const io = new Server(server)

app.use((req, res, next) => {
    console.log(`${req.method} ${req.url}`)
    next()
})

app.use(express.static('.'))

app.get('/', (req, res) => {
    res.sendFile('index.html')
})

io.on('connect', socket => {
    console.log(`id: ${socket.id} connected`)

    socket.on('message', msg => {
        io.send(msg)
    })
    socket.on('enter',  (user, id) => {
        io.emit('enter', user, id)
        socket.on('disconnect', () => {
            console.log('user disconnected ' + socket.id)
            io.emit('leave', socket.id)
        })
    })
})

server.listen(3000, () => {
    console.log('server listening on http://localhost:3000/')
})
