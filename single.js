const LOREM = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'

function tag(name, ...children) {
    const _ = document.createElement(name)
    _.attr = (name, value) => {
        _.setAttribute(name, value)
        return _
    }
    _.addClass = name => {
        _.classList.add(name)
        return _
    }
    _.setStyle = (name, value='', priority='') => {
        _.style.setProperty(name, value, priority)
        return _
    }
    _.append(...children)
    return _
}

const MUNDANE_TAGS = ['article', 'aside', 'blockquote', 'div', 'footer', 'h1',
'h2', 'h3', 'h4', 'h5', 'h6', 'header', 'main', 'nav', 'p', 'section', 'span',
'textarea', 'br', 'b', 'details', 'summary', 'ul', 'li', 'script', 'title',
'hr', 'form', 'button']

for (let tagName of MUNDANE_TAGS) {
    window[tagName] = (...children) => tag(tagName, ...children)
}

function a(href, ...children) {
    return tag('a', ...children).attr('href', href)
}

function img(src) {
    return tag('img').attr('src', src)
}

function input(type) {
    return tag('input').attr('type', type)
}

function style(css) {
    return tag('style', css)
}

function link(href, rel='stylesheet') {
    return tag('link').attr('rel', rel).attr('href', href)
}

function shadow(name, ...children) {
    const _ = tag(name)
    _.attachShadow({mode: 'open'}).append(...children)
    return _
}

function slugify(str) {
    return str.trim()
        .replace(/[^\w\s-.~]/g, '')
        .replace(/\s/g, '_')
}

function getPath() {
    return location.hash.slice(1)
}

function route(routes) {
    function resolve() {
        const path = getPath()
        for (let pattern in routes) {
            const match = path.match(new RegExp(`^${pattern}$`))
            if (match) {
                // console.log(`${path} matched: ${pattern}`)
                routes[pattern](match)
                return
            }
        }
    }
    window.addEventListener('load', resolve)
    window.addEventListener('hashchange', resolve)
}
